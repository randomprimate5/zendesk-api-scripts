import csv
import requests
import api_connect

# Set the request parameters
users_url = api_connect.url + 'users.json'
users = []

# Start session
session = requests.Session()
session.auth = (api_connect.user, api_connect.token)

# Paginated call
while users_url:
    response = session.get(users_url)
    data = response.json()
    for ticket in data['users']:
        users.append(ticket)
    print(users_url)
    users_url = data['next_page']

# Title Row
title_row = ["id", "url", "name", "created_at", "updated_at", "email", "role" ]

print("Got results. Starting to write on file.")

with open('../data/users.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(title_row)
    for inst in users:
        values = [
            inst["id"], inst["url"], inst["name"], inst["created_at"], inst["updated_at"], inst["email"], inst["role"]]
        writer.writerow(values)
    print('User count: ' + str(data['count']))

print("Done!")
