import csv
import requests
import api_connect

# Note: we can add more views and deactivate them to avoid overcrowding the ZD dashboard but still pull them here.

# Set the request parameters
channels_url = api_connect.url + 'views/active.json'
views_url = api_connect.url + 'views/count_many.json?ids='
views_id = []
view_count = []

# Views that represent channels Reddit is full of comments and emergency and security has pendings as well.
views_of_interest = [
    'Emergency & Security',
    'Quora',
    'Disqus Comments',
    'Twitter (@gitlabsupport, @gitlabstatus) and Facebook',
    '@gitlab Tweets',
    'StackOverflow',
    'Pivotal CF',
    'Mailing List',
    'GitLab Forum',
    'Support Forum',
    'GitHost.io',
    'Reddit'
]

# Do the HTTP get request
response = requests.get(channels_url, auth=(api_connect.user, api_connect.token))

# Check for HTTP codes other than 200
if response.status_code != 200:
    print('Status: ', response.status_code, 'Problem with the request. Exiting.')
    exit()

# Decode the JSON response into a dictionary and use the data
data = response.json()

views_list = data['views']

# Console output
for view in views_list:
    title_exists = view['title'] in views_of_interest
    print(view['title'])
    if title_exists:
        views_id.append(view['id'])

        # Get ticket count per view
        count_url = views_url + str(view['id'])
        view_response = requests.get(count_url, auth=(api_connect.user, api_connect.token))
        view_data = view_response.json()

        # Can we make this an object? With title, count, and id.
        view_count.append([view['title'], view_data['view_counts'][0]['value']])


with open('views.csv', 'w') as csvfile:
    view_writer = csv.writer(csvfile, delimiter=',')
    view_writer.writerow(['View', 'Tickets'])
    for inst in view_count:
        view_writer.writerow(inst)

print("Done.")
