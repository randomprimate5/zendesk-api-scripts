import requests
import api_connect

# Set url
orgs_url = api_connect.url + 'organizations/'
org_id = '30538447'
an_org = orgs_url + org_id + '.json'
orgs = []

# Start session
session = requests.Session()
session.auth = (api_connect.user, api_connect.token)

# Get results
response = session.get(an_org)
data = response.json()
print(data)
