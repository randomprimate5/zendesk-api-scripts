import csv
import requests
import api_connect

# Set url
orgs_url = api_connect.url + 'organizations.json'
orgs = []

# Start session
session = requests.Session()
session.auth = (api_connect.user, api_connect.token)

# Paginated call
while orgs_url:
    response = session.get(orgs_url)
    data = response.json()
    for org in data['organizations']:
        orgs.append(org)
    print(orgs_url)
    orgs_url = data['next_page']

# Title Row
title_row = [
    "id", "name", "domains"
]

print("Got results. Starting to write on file.")

with open('../data/orgs.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(title_row)
    for inst in orgs:
        values = [
            inst["id"], inst["name"], inst["domain_names"]
        ]
        writer.writerow(values)
    print('Org count: ' + str(data['count']))

print("Done!")