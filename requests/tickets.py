import csv
import requests
import api_connect

# Set url
ticket_url = api_connect.url + 'tickets.json?include=comment_count'
tickets = []

# Start session
session = requests.Session()
session.auth = (api_connect.user, api_connect.token)

# Paginated call
while ticket_url:
    response = session.get(ticket_url)
    data = response.json()
    for ticket in data['tickets']:
        tickets.append(ticket)
    print(ticket_url)
    url = data['next_page']

# Title Row
title_row = ["id", "url", "external_id", "created_at", "updated_at", "type", "subject", "raw_subject", "description",
             "priority", "status", "recipient", "requester_id", "submitter_id", "assignee_id", "organization_id",
             "group_id", "collaborator_ids", "forum_topic_id", "problem_id", "has_incidents", "due_at", "tags", "via",
             "custom_fields", "satisfaction_rating", "sharing_agreement_ids"
             ]

print("Got results. Starting to write on file.")

with open('../data/tickets.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(title_row)
    for inst in tickets:
        values = [
            inst["id"], inst["url"], inst["external_id"], inst["created_at"], inst["updated_at"], inst["type"],
            inst["subject"], inst["raw_subject"], inst["description"], inst["priority"], inst["status"],
            inst["recipient"], inst["requester_id"], inst["submitter_id"], inst["assignee_id"], inst["organization_id"],
            inst["group_id"], inst["collaborator_ids"], inst["forum_topic_id"], inst["problem_id"],
            inst["has_incidents"], inst["due_at"], inst["tags"], inst["via"], inst["custom_fields"],
            inst["satisfaction_rating"], inst["sharing_agreement_ids"]
        ]
        print(values)
        writer.writerow(values)
    print('Ticket count: ' + str(data['count']))

print("Done!")
