import requests
import api_connect

# Set the request parameters
groups_url = api_connect.url + 'groups.json'

# Do the HTTP get request
response = requests.get(groups_url, auth=(api_connect.user, api_connect.token))

# Check for HTTP codes other than 200
if response.status_code != 200:
    print('Status: ', response.status_code, 'Problem with the request. Exiting.')
    exit()

# Decode the JSON response into a dictionary and use the data
data = response.json()

# Example 1: Print the name of the first group in the list
print('First group = ', data['groups'][0]['name'])
#print(data)

# Example 2: Print the name of each group in the list
group_list = data['groups']
for group in group_list:
    print(group['name'])

# Example 3: Save to file
output = ''

for group in group_list:
    output += group['name'] + '\n'  # add each name to the output variable

with open('groups.txt', mode='w', encoding='utf-8') as f:
    f.write(output)

print("Done.")
